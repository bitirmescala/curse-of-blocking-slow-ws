package controllers

import play.api._
import play.api.mvc._
import akka.pattern.after
import scala.concurrent.duration._
import play.api.libs.concurrent.Akka
import play.api.Play.current
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

object Application extends Controller {

  
  def pingWithDelay = Action.async {
    after(500 millis, using = Akka.system.scheduler)(Future.successful{
      
      val rand = Random.alphanumeric.take(10).mkString
      Ok(rand+ "\n")
      
    })
    
  }

}